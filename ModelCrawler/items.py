# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ModelcrawlerItem(scrapy.Item):
    # define the fields for your item here like:
    model_name = scrapy.Field()
    coordi_url = scrapy.Field()
    model_face = scrapy.Field()
    height = scrapy.Field()
    weight = scrapy.Field()
    image_urls = scrapy.Field()
    created_at = scrapy.Field()
    views = scrapy.Field()
    brand_stores = scrapy.Field()
    rel_products = scrapy.Field()  # list of product urls

    #hiphoper
    model_job = scrapy.Field()
    model_age = scrapy.Field()
    location = scrapy.Field()
    sns_id = scrapy.Field()
    style = scrapy.Field()
    coordi_comment = scrapy.Field()
    likes = scrapy.Field()

