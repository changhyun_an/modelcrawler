# -*- coding: utf-8 -*-
import os
import re
import json
import scrapy
from ModelCrawler.items import ModelcrawlerItem as Model
try:
    import urllib.parse as urlparse
except ImportError:
    import urllib as urlparse



class MusinsaModel(scrapy.Spider):
    name = "musinsa_model"
    allowed_domain = ["musinsa.com"]
    base = "http://store.musinsa.com/app/styles/lists"
    start_urls = [base]
    failed_urls = []
    models_info = []

    def parse(self, response):
        """
        년도 범위를 찾아서 request
        :param response:
        :return:
        """
        max_year = response.xpath('//input[@name="max_rt"]/@value').extract_first()
        min_year = response.xpath('//input[@name="min_rt"]/@value').extract_first()
        print(min_year, max_year)
        for yr in range(int(min_year), int(max_year)+1):
            url = ''.join([self.base, '?year_date=', str(yr)])
            yield scrapy.Request(url, callback=self.parse_pages)

    def parse_pages(self, response):
        """
        최대 페이지 수를 가져와서 그만큼 request
        :param response:
        :return:
        """
        total = int(response.css('span.pagingNumber span.totalPagingNum::text').extract_first().strip())
        for p in range(total):
            url = ''.join([response.url, '&page=', str(p+1)])
            yield scrapy.Request(url, callback=self.parse_list)

    def parse_list(self, response):
        """
        페이지 내에 주소를 가지고 request
        :param response:
        :return:
        """
        for js in response.css('div.list-box.box ul li div.list_img.opacity_img a').xpath('@onclick').extract():
            id = re.search(r'\(\'(.*)\'\)', js).group(1)
            url = ''.join([self.base.split('lists')[0], 'views/', id])
            yield scrapy.Request(url, callback=self.parse_model)

    def parse_model(self, response):
        """
        모델 페이지 내에서 정보를 가져와서 모델 객체에 셋팅 후
        리스트에 추가
        :param response:
        :return:
        """
        try:
            model = Model()
            model['model_name'] = self.parse_model_name(response)
            model['coordi_url'] = self.parse_coordi_url(response)
            model['model_face'] = self.parse_model_face(response)
            model['height'], model['weight'] = self.parse_model_size(response)
            model['image_urls'] = self.parse_image_urls(response)
            model['created_at'] = self.parse_created_time(response)
            model['views'] = self.parse_coordi_views(response)
            model['brand_stores'] = self.parse_brands(response)
            model['rel_products'] = self.parse_related_products(response)

            self.models_info.append((dict(model)))
            yield model

        except Exception as e:
            print(e)
            self.failed_urls.append(response.url)

    def parse_model_name(self, response):
        """
        모델 이름 파싱
        :param response:
        :return:
        """
        return response.css('div.box_model::text').extract_first().strip()

    def parse_coordi_url(self, response):
        """
        코디 게시물 주소 파싱
        :param response:
        :return:
        """
        return response.url

    def parse_model_face(self, response):
        """
        모델 얼굴 사진 파싱
        :param response:
        :return:
        """
        img_url = response.css('div.box_model_img img').xpath('@src').extract_first()
        return 'http:' + img_url if not 'http:' in img_url else img_url

    def parse_model_size(self, response):
        """
        모델 신체사이즈 파싱
        :param response:
        :return:
        """
        body_info = response.css('div.box_model span.txt_size_model::text').extract_first()
        return re.search(r'\((.*)\)', body_info).group(1).split(' / ')

    def parse_image_urls(self, response):
        """
        코디 이미지(들) 파싱
        "http:"가 없는 주소는 고쳐서 추가
        :param response:
        :return:
        """
        image_base = 'http://images.musinsa.com'
        parsed_urls = response.css('div.curating_text_area.curating_area_contents p img').xpath('@src').extract()
        img_urls = []
        for url in parsed_urls:
            if not 'http:' in url:
                if '//' in url:
                    img_urls.append('http:' + url)
                else:
                    img_urls.append(image_base + url)
            else:
                img_urls.append(url)

        return img_urls

    def parse_created_time(self, response):
        """
        코디 게시물이 게시된 날짜 파싱
        :param response:
        :return:
        """
        time_info = response.css('p.box_date_style::text').extract_first().split(' / ')[0]
        return re.split('\[.*\]', time_info)[0].strip()

    def parse_coordi_views(self, response):
        """
        해당 코디 게시물의 조회 수 파싱
        :param response:
        :return:
        """
        view_info = response.css('p.box_date_style::text').extract_first().split(' / ')[1]
        return int(view_info.split(': ')[1].replace(',',''))

    def parse_brands(self, response):
        """
        코디 아이템의 브랜드숍들 파싱
        :param response:
        :return:
        """
        return [br.strip() for br in response.css('p.box_brand_style a::text').extract()]

    def parse_related_products(self, response):
        """
        코디 아이템들의 정보 파싱. 이름, 주소, 이미지주소
        :param response:
        :return:
        """
        prd_info = [] # list of [prd_name, prd_url, image_url]
        url_base = '{uri.scheme}://{uri.netloc}'.format(uri=urlparse.urlparse(self.base))
        rel_products = response.css('div#rel_product ul li.li_box')
        for prd in rel_products:
            prd_dict = {}
            prd_dict['product_name'] = prd.css('div.list_img.articleImg a img').xpath('@alt').extract_first()
            prd_dict['product_url'] = url_base + prd.css('div.list_img.articleImg a').xpath('@href').extract_first()
            img_url = prd.css('div.list_img.articleImg a img').xpath('@src').extract_first()
            prd_dict['image_url'] =  'http:' + img_url if not 'http:' in img_url else img_url

            prd_info.append(prd_dict)

        return prd_info

    def close(spider, reason):
        """
        크롤러 종료 시, 실패한 주소 반환 및 코디모델 정보는 json 파일로 저장
        :param reason:
       :return:
        """
        print("failed urls: ", spider.failed_urls)

        dir = os.path.dirname(__file__)
        filename = os.path.join(dir, '../musinsa_coordi_models.json')
        with open(filename, mode='w', encoding='utf-8') as f:
            json.dump(spider.models_info, f, indent=4, ensure_ascii=False, sort_keys=True)







