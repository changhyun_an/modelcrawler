# -*- coding: utf-8 -*-
import os
import json
import scrapy
from urllib.parse import urlencode

from ModelCrawler.items import ModelcrawlerItem as Model

class HiphoperModelCrawler(scrapy.Spider):
    name = "hiphoper_model"
    allowed_domain = "hiphoper.com"
    base = "http://www.hiphoper.com/maz/board.php?bo_table=street"
    start_urls = [base]
    model_data = []
    failed_urls = []

    def parse(self, response):
        """
        힙합퍼는 0000년도, 00월, 0주차로 나눠져있다
        그리서 주차별로 파싱을 해야한다
        :param response:
        :return:
        """
        default_year = 2000
        months_count = 12
        weeks_count = 5
        years_count = len(response.css('div.list-buttons a').extract())
        params = {} # for url
        for y in range(years_count):
            params['year'] = str(default_year + y)
            for m in range(months_count):
                params['mon'] = m + 1
                for w in range(weeks_count):
                    params['week'] = w + 1
                    url = ''.join([response.url, '&', urlencode(params)])

                    yield scrapy.Request(url, callback=self.parse_list)
        #yield scrapy.Request(response.url+'&mon=6&week=1&year=2016', callback=self.parse_list)

    def parse_list(self, response):
        if not response.xpath('//li[@class="empty_list"]'):
            urls = response.css('ul#flexlist li a').xpath('@href').extract()
            for url in urls:
                yield scrapy.Request(url, callback=self.parse_model)

    def parse_model(self, response):
        try:
            model = Model()
            main_info = response.css('div.street-main-body ul')
            model['model_name'], model['model_job'], model['model_age'] = self.parse_model_info(info=main_info)
            model['location'], model['style'], model['sns_id'], model['coordi_comment'] = \
                self.parse_detail_info(info=main_info)
            model['created_at'] = self.parse_created_at(response)
            model['views'], model['likes'] = self.parse_views_likes(response)
            model['coordi_url'] = response.url
            model['image_urls'] = self.parse_image_urls(response)
            model['rel_products'] = self.parse_rel_products(response)

            self.model_data.append(dict(model))
            yield model

        except Exception as e:
            print(e)
            self.failed_urls.append(response.url)

    def parse_model_info(self, info):
        model_info = info.css('li:first-of-type::text').extract_first().strip().split('/')  # ['김은지', '모델', 24]
        return [None if not i or not i.strip() else i.strip() for i in model_info]

    def parse_detail_info(self, info): # location, style, sns, coordi comment
        details = [info.css('li:nth-of-type({nth})::text'.format(nth=i)).extract_first().strip() for i in range(2,6)]
        return [None if not i or not i.strip() else i.strip() for i in details]

    def parse_created_at(self, response):
        return '20' + ''.join(response.css('div.view_datetime::text').extract()).strip().split()[0].replace('-', '.')

    def parse_views_likes(self, response):
        return [int(response.css('div.view_hit {ele}'.format(ele=ele)).xpath('../text()')
                    .extract_first().strip().replace(",", '')) for ele in ('i.fa-eye', 'i.fa-thumbs-o-up')]

    def parse_image_urls(self, response):
        return response.css('div.street_contents div.mainpic img').xpath('@src').extract()

    def parse_rel_products(self, response):
        prd_info = []

        rel_products = response.css('div.itempic + div')
        for prd in rel_products:
            product_name = prd.css('div.widget-main h5::text').extract_first().strip()
            for img in prd.css('div.widget-main-body div a'):
                prd_dict = {}
                prd_dict['product_name'] = product_name
                prd_dict['image_url'] = img.xpath('img/@src').extract_first()
                if not prd_dict['image_url']:
                    continue
                prd_dict['product_url'] = img.xpath('@href').extract_first().strip()

                prd_info.append(prd_dict)

            if not [item for item in prd_info if item['product_name'] == product_name]:
                prd_dict = {}
                prd_dict['product_name'] = product_name
                prd_dict['image_url'] = response.css('div.itempic img').xpath('@src').extract_first().strip()
                prd_dict['product_url'] = None
                prd_info.append(prd_dict)

        return prd_info

    def close(spider, reason):
        print("failed_urls:", spider.failed_urls)
        dir = os.path.dirname(__file__)
        filename = os.path.join(dir, '../hiphoper_coordi_models.json')

        with open(filename, 'w') as f:
            json.dump(spider.model_data, f, indent=4, ensure_ascii=False, sort_keys=True)