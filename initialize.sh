#!/bin/bash
python3 -m venv ./.venv
source $PWD/.venv/bin/activate
$PWD/.venv/bin/pip3 install -r $PWD/requirements.txt