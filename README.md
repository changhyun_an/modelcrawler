# ModelCrawler #


### Set up ###

* ```$. initialize.sh``` to set up virtual environment and dependencies
* Refer to ```/requirements.txt``` for detailed dependencies
* To Run Crawler, ```$scrapy crawl musinsa_model```


### Crawled Data Format ###

* Crawled data is saved as json in ```/ModelCrawler/musinsa_coordi_model.json```
* Properties include, 
```javascript
[
    {
        "brand_stores": [],
        "coordi_url": "http://store.musinsa.com/app/styles/views/356",
        "created_at": "2013.02.28",
        "height": "176cm",
        "image_urls": [
            "http://image.musinsa.com/images/style/content/2013022815044900000059596.jpg",
            "http://image.musinsa.com/images/style/content/2013022815045000000043657.jpg",
            "http://image.musinsa.com/images/style/content/2013022815045100000054009.jpg",
            "http://image.musinsa.com/images/style/content/2013022815045200000094371.jpg",
            "http://image.musinsa.com/images/style/content/2013022815045400000060222.jpg"
        ],
        "model_face": "http://image.musinsa.com/images/style_model/1346830843_7f4d6e4e8520f3626cfb253bbdc5c1ef.jpg",
        "model_name": "허정",
        "rel_products": [],
        "views": 2337,
        "weight": "57kg"
    }, ...
```
